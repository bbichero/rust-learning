struct ImportantExcerpt<'a> {
    part: &'a str,
}

fn longuest<'a>(str1: &'a str, str2: &'a str) -> &'a str {
    if str1.len() > str2.len() {
        str1
    } else {
        str2
    }
}

fn main() {
    //let str1 = String::from("yolo1");
    //let str2 = "yolo11111";

    //println!("the longest string is: {}", longuest(str1.as_str(), str2));

    let string2 = String::from("qqwe");
    let string1 = String::new();
    let result = longuest(string1.as_str(), string2.as_str());
    {
        //string1 = String::from("long string very long");
    }
    println!("the longuest string is: {}", result);

    let novel = String::from("Call me yolo. some years ago...");
    let first_sentence = novel.split('z')
        .next()
        .expect("Could not find a '.'");
    let i = ImportantExcerpt { part: first_sentence };
    println!("first sentence: {}", first_sentence);
    println!("{}", i.part);
}
