fn main() {
    let space = "  ";
    let space = space.len();

    let ret :bool = false;
    let tup: (char, bool, u32) = ('c', true, 32);

    let arr = ["123", "345"];
    let index = 10;
    let element = arr[index];

    println!("the value of space is {}", space);
    println!("the value of ret is {}", ret);
    println!("the value of tup is {}", tup.0);
    println!("the value of arr is {}", element);
}
