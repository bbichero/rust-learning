pub trait Summary {
    fn summarize(&self) -> String {
        String::from("(Read more...)")
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
} 

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("username dela mort"),
        content: String::from("Of course you no"),
        reply: false,
        retweet: false,
    }
}

fn main() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from("salut, content super c est cool"),
        reply: false,
        retweet: false,
    };

    let new_article = NewsArticle {
        headline: String::from("Headline wao"),
        location: String::from("France"),
        author: String::from("Jean paul"),
        content: String::from("Super article, c'est cool, tout le monde le lis"),
    };
    println!("1 new tweet: {}", tweet.summarize());
    println!("1 new article: {}", new_article.summarize());
}
