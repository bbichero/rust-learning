fn main() {

    let v: Vec<i32> = Vec::new();
    let mut v = vec![1, 2, 3, 4, 5];

    //let mut v = Vec::new();
    //v.push(5);

    let third: &i32 = &v[2];
    println!("the third element is {}", third);

    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("there is no third element"),
    }

    let mut v = vec![12, 34, 56];

    for i in &mut v {
        *i += 1;
        println!("{}", i);
    }
    let data = "initial content";

    let s = data.to_string();
    let s = "initial content".to_string();

    let mut s1 = String::from("foo");
    let s2 = "far";
    s1.push_str(s2);
    s1.push('9');
    println!("{} {}", s1, s2);

    let s1 = String::from("hello, ");
    let s2 = String::from("world");
    let s3 = String::from(" gogoglel");
    let s = format!("{}-{}-{}", s1, s2, s3);

    for c in "yoloooo".chars() {
        println!("{}", c);
    }

    for c in "yoloooo".bytes() {
        println!("{}", c);
    }

    println!("{}", s);

    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 40);

    let team_name = String::from("Blue");
    let score = scores.get(&team_name);

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();

    let field_name = String::from("str 1");
    let field_value = String::from("str 2");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);

    scores.entry(String::from("Yellow")).or_insert(45);
    scores.entry(String::from("Blue")).or_insert(12);

    let text = "hello world ownderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }

    println!("{:?}", map);
}
