use std::io;
use rand::Rng;
use std::cmp::Ordering;

const GUESS_MIN : i32 = 0;
const GUESS_MAX : i32 = 100;

pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < GUESS_MIN || value > GUESS_MAX {
            panic!("Guess must be between {} and {}, got {}", GUESS_MIN, GUESS_MAX, value);
        }

        Guess {
            value
        }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    //println!("The secret number is: {}", secret_number);

    loop {
        let mut guess = String::new();

        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        if guess.trim() == String::from("exit") {
            break;
        }

        let to_int :i32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(err) => {
                println!("{}", err);
                continue;
            },
        };
        let guess :i32 = Guess::new(to_int).value();

        println!("Please input your guess.");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
