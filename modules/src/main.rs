mod plant {
    pub struct Vegetable {
        pub name: String,
        id: i32,
    }

    impl Vegetable {
        pub fn new(name: &str) -> Vegetable {
            Vegetable {
                name: String:: from(name),
                id: 1,
            }
        }
    }
}
mod sound;

fn main() {
    println!("Hello, world!");
    crate::sound::instrument::flute(2);
    sound::instrument::flute(2);

    let mut v = plant::Vegetable::new("squash");

    v.name = String::from("butternut squash");
    println!("{} are delicious", v.name);
}
