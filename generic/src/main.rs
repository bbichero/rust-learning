//fn largest_i32(list: &[i32]) -> i32 {
//
//    let mut largest = list[0];
//
//    for &number in list.iter() {
//        if number > largest {
//            largest = number;
//        }
//    }
//
//    largest
//}

fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    largest
}

struct Point<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point<T, U> {
    fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
    fn x(&self) -> &T {
        &self.x
    }
}

//impl Point<f32> {
//    fn distance_from_origin(&self) -> f32 {
//        (self.x.powi(2) + self.y.powi(2)).sqrt()
//    }
//}

fn main() {
    let number_list = vec![23, 43, 21, 6, 89];

    //let result = largest_i32(&number_list);
    let result = largest(&number_list);
    println!("largest number is: {}", result);

    let char_list = vec!['f', 'f', 'd', 'r'];

    let result = largest(&char_list);
    println!("largest number is: {}", result);

    let integer = Point { x: 5, y: 10 };

    println!("p.x = {}", integer.x());

    let p1 = Point { x: 5, y: 10.4 };
    let p2 = Point { x: "hello", y: 'c' };
    let p3 = p1.mixup(p2);

    println!("p3.x = {}, p3.y = {}", p3.x, p3.y);
}
