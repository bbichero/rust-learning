use std::fs::File;
use std::io::ErrorKind;

use std::io::{self, Read, fs};

fn main() {
    let v = vec![1, 2, 3];

    //v[99];
    println!("Hello, world!");

    //let f = File::open("hello.txt");

//    let f = match f {
//        Ok(file) => file,
//        Err(error) => match error.kind() {
//            ErrorKind::NotFound => match File::create("hello.txt") {
//                Ok(fc) => fc,
//                Err(e) => panic!("Tried to create file but there was a problem: {:?}", e),
//            },
//            other_error => panic!("can't open file: {}", error),
//        },
//    };
//
//    let f = File::open("hello.txt").unwrap_or_else(|error| {
//        if error.kind() == ErrorKind::NotFound {
//            File::create("hello.txt").unwrap_or_else(|error| {
//                panic!("Tried to create file but error occured: {:?}", error);
//            })
//        } else {
//            panic!("Error when opening file: {:?}", error);
//        }
//    });

    //let f = File::open("hello.txt").unwrap();
    //let f = File::open("hello.txt").expect("failed to open hello.txt");

    match read_username_from_file_3() {
        Ok(username) => println!("{}", username),
        Err(e) => println!("error: {}", e),
    };
}

fn read_username_from_file() -> Result<String, io::Error> {
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();
    println!("{}", s);

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

fn read_username_from_file_2() -> Result<String, io::Error> {
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}

fn read_username_from_file_3() -> Result<String, io::Error> {
    let mut s = String::new();

    File::open("hello.txt")?.read_to_string(&mut s)?;

    Ok(s)
}

fn read_username_from_file_4() -> Result<String, io::Error> {
    fs::read_to_string("hello.txt")
}
