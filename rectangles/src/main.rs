#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, rec: &Rectangle) -> bool {
        self.width > rec.width && self.height > rec.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle { width: size, height: size }
    }
}

fn main() {
    let rec1 = Rectangle { width: 50, height: 30 };
    let rec2 = Rectangle { width: 40, height: 20 };
    let rec3 = Rectangle { width: 30, height: 10 };

    println!("can rec1 hold rec2 ? {}", rec1.can_hold(&rec2));
    println!("can rec2 hold rec3 ? {}", rec2.can_hold(&rec3));
    let rec4 = Rectangle::square(1);
    println!("{:?}", rec4);
}
