fn main() {
    let mut s = String::from("hello");

    s.push_str(" world !");
    println!("{}", s);

    let mut x = 5;
    let mut y = x;

    x -= 1;
    y += 1;

    println!("x = {}, y = {}", x, y);

    let s1 = String::from("string");
    let mut s2 = s1.clone();

    s2.push_str(" super");

    println!("s1 = {}, s2 = {}", s1, s2);

    let s1 = gives_ownership();

    let s2 =  String::from("hello");

    let mut s3 = takes_and_gives_back(s2);

    //let len = calculate_len(&s3);
    //change(&mut s3);

    let r1 = &s3;
    let r2 = &s3;
    //let r3 = &mut s3;

    //println!("{}, {}, {}", r1, r2, r3);
    
    let new = dangle();

    println!("first_word('yoloo') = {}", first_word(&String::from("yoloooo qweqwe  qwe")));

    let mut s = String::from("Hello world");

    let word = first_word(&s);

    println!("word = {}", word);

    let s = String::from("hello world");

    let hello = &s[..=4];
    let world = &s[6..];

    println!("{} {}", hello, world);

    println!("==================");
    let my_string = String::from("hello world");

    let word = first_word(&my_string[..]);
    println!("word = {}", word);

    println!("==================");
    let my_string_literal = "hello world";

    let word = first_word(&my_string_literal[..]);
    println!("word = {}", word);
    let word = first_word(my_string_literal);
    println!("word = {}", word);
}

fn dangle() -> String {
    let s = String::from("yolo");

    s
}

fn first_word(s :&str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}

fn change(string: &mut String) {
    string.push_str(", world");
}

fn calculate_len(s: &String) -> usize {
    s.len()
}

fn gives_ownership() -> String {
    let some_string = String::from("hello");
    some_string
}

fn takes_and_gives_back(a_string: String) ->String {
    a_string
}
