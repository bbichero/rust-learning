struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn color_point(black: Color) -> Point {
    Point(1, 3, 4)
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn main() {

    let mut user1 = build_user(String::from("super@email.com"), String::from("username"));

    user1.email = String::from("super2@email.com");

    let user2 = User {
        email: String::from("super2@email.com"),
        username: String::from("usernamedelamort"),
        ..user1
    };

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);

    let dot = color_point(black);
}
